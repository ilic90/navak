<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->integer('section_id')->unsigned()->index();
            $table->integer('position');
            $table->boolean('custom_answer')->default(0);
            $table->boolean('multiple_answers')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_questions');
    }
}
