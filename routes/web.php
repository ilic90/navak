<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vendor.adminlte.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Register routes disabled
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
 });

Route::group(['middleware' => 'auth'], function(){
    // User routes
    Route::get('/users', 'UserController@index');
    Route::get('/create/user', 'UserController@create');
    Route::get('/edit/user/{id}', 'UserController@edit')->name('edit.user');
    Route::get('/delete/user/{id}', 'UserController@destroy')->name('delete.user');
    Route::post('/store/user', 'UserController@store')->name('store.user');
    Route::post('/update/user/{id}', 'UserController@update')->name('update.user');
});
