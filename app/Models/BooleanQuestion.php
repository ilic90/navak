<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BooleanQuestion extends Model
{
    
    protected $fillable = [
        'question', 'section_id', 'position',
    ];

    public function booleanAnswers()
    {
        return $this->hasMany(BooleanAnswer::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

}
