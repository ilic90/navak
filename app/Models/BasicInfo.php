<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicInfo extends Model
{
    
    protected $fillable = [
        'date', 'training',
    ];

    public function booleanAnswers()
    {
        return $this->hasMany(BooleanAnswer::class);
    }

    public function markAnswers()
    {
        return $this->hasMany(MarkAnswer::class);
    }

    public function textAnswers()
    {
        return $this->hasMany(TextAnswer::class);
    }

}
