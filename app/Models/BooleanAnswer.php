<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BooleanAnswer extends Model
{
    
    protected $fillable = [
        'answer', 'boolean_question_id', 'basic_info_id',
    ];

    public function basicInfo()
    {
        return $this->belongsTo(BasicInfo::class);
    }

    public function booleanQuestion()
    {
        return $this->belongsTo(BooleanQuestion::class);
    }

}
