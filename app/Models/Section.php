<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    
    protected $fillable = [
        'name', 'position',
    ];

    public function booleanQuestions()
    {
        return $this->hasMany(BooleanQuestion::class);
    }

    public function markQuestions()
    {
        return $this->hasMany(MarkQuestion::class);
    }

    public function textQuestions()
    {
        return $this->hasMany(TextQuestion::class);
    }

}
