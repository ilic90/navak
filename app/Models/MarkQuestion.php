<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarkQuestion extends Model
{
    
    protected $fillable = [
        'question', 'section_id', 'position',
    ];

    public function markAnswers()
    {
        return $this->hasMany(MarkAnswer::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

}
