<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarkAnswer extends Model
{
    
    protected $fillable = [
        'answer', 'mark_question_id', 'basic_info_id',
    ];

    public function basicInfo()
    {
        return $this->belongsTo(BasicInfo::class);
    }

    public function markQuestion()
    {
        return $this->belongsTo(MarkQuestion::class);
    }

}
