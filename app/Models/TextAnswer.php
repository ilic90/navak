<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextAnswer extends Model
{
    
    protected $fillable = [
        'answer', 'text_question_id', 'basic_info_id',
    ];

    public function basicInfo()
    {
        return $this->belongsTo(BasicInfo::class);
    }

    public function textQuestion()
    {
        return $this->belongsTo(TextQuestion::class);
    }

}
