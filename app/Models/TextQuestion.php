<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextQuestion extends Model
{
    
    protected $fillable = [
        'question', 'section_id', 'position', 'custom_answer', 'multiple_answers'
    ];

    public function textAnswers()
    {
        return $this->hasMany(TextAnswer::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function offeredAnswers()
    {
        return $this->hasMany(OfferedAnswer::class);
    }

}
