<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferedAnswer extends Model
{
    
    protected $fillable = [
        'answer', 'text_question_id',
    ];

    public function textQuestion()
    {
        return $this->belongsTo(TextQuestion::class);
    }

}
